import unittest
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO


def failed_function():
    print("Starting the failed_function")
    x = 1/0
    return x


class SampleTests(unittest.TestCase):
    def test_exception(self):
        print("Starting the testcase")
        self.assertRaises(ZeroDivisionError, failed_function)


if __name__ == '__main__':
    std_out = StringIO()
    runner = unittest.TextTestRunner(stream=std_out)
    test_results = runner.run(unittest.makeSuite(SampleTests))
    std_out.seek(0)
    print("Test Output:", std_out.read())
