import unittest


class TestskipTest(unittest.TestCase):
    def setUp(self): print("> \tsetup")
    
    def tearDown(self): print("> \tteardown")

    def test_skiping(self): print("# test_skiping")

    def test_run(self): print("Should run")


if __name__ == "__main__":
    import sys
    mods = sys.modules['__main__']
    suit = unittest.loader.defaultTestLoader.loadTestsFromModule(mods)
    for tcs in suit:
        for t in tcs:
            if 'ing' in t.id():
                setattr(t, 'setUp', lambda: t.skipTest('Set to Skip'))
    unittest.TextTestRunner().run(suit)

