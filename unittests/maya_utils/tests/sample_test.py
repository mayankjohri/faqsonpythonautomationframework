import unittest


def failed_function():
    x = 1/0
    return x


class SampleTests(unittest.TestCase):
    def test_exception(self):
        self.assertRaises(ZeroDivisionError, failed_function)


if __name__ == '__main__':
    unittest.main()
