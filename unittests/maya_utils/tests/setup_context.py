from contextlib import contextmanager
import unittest


@contextmanager
def setup_teardown():
    print("Inside setup")
    yield 'mayank'
    print("Inside teardown")


class ContextTest(unittest.TestCase):

    def run(self, result=None):
        with setup_teardown() as pre_post:
            self.pre_post = pre_post
            super(ContextTest, self).run(result)

    def test(self):
        print("Starting the testing")
        self.assertEqual('mayank', self.pre_post)


if __name__ = "__main__":
    unittest.main(verbosity=2)
