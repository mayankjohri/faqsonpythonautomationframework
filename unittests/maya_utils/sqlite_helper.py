"""."""


import sqlite3


class SH():
    """."""

    def __init__(self, db_file):
        """."""

        self.conn = sqlite3.connect(db_file)
        self.cursor = self.conn.cursor()

    def __del__(self):
        self.cursor.close()
        self.conn.close()

    def query(self, query, *vals):
        """."""

        try:
            if query.lower().startswith(("insert", "update")):
                self.cursor.execute(query, vals)
            else:
                return self.cursor.execute(query, vals).fetchall(), None
        except Exception as ex:
            return None, ex
        return None, None
