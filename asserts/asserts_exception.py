import unittest


class TestAssertException(unittest.TestCase):

    def test_exception(self):
        with self.assertRaises(ZeroDivisionError):
            x = 1/0


if __name__ == '__main__':
    unittest.main()
