""" Tests asserts an exception
"""
import pytest

def test_assert_exception():
    """Validates the ZeroDivisionError"""

    with pytest.raises(ZeroDivisionError):
        x = 1 / 0
