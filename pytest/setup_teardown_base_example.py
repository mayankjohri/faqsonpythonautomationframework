import pytest

class TestSample(object):
    @classmethod
    def setup_class(cls):
        print("Setup: should only run once")

    @classmethod
    def teardown_class(cls):
        print("Teardown: should only run once")

    def test_one(self):
        print("This is test_one")
        assert(True)

    def test_two(self):
        print("This is test_two")
        assert(True)

