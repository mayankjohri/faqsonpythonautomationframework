import pytest
from _pytest.capture import capsys


def printme(capsys, txt):
    with capsys.disabled():
        print(txt)


def test_one(capsys):
    print("This will not print")
    printme(capsys, "This should print")
    assert(True)

