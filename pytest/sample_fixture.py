"""."""

import pytest


@pytest.fixture(scope='function')
def x_val(request):
    """fixture x """
    return request.param * 3


@pytest.fixture(scope='function')
def y_val(request):
    """Another Test"""
    return request.param * 2


@pytest.mark.parametrize('x_val, y_val', [('a', 'b')], indirect=['x_val'])
def test_indirect(x_value, y_value):
    """Dummy doc"""
    assert x_value == 'aaa'
    assert y_value == 'b'
